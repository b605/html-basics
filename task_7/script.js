function generate_shapes(n, type) {

    var max = 32;
    var min = 8;

    var max_pos = 2000;
    var min_pos = 50;
    
    if (type == "S") {
        var shape_type_class = "square";
    }
    else if (type == "T") {
        var shape_type_class = "triangle";
    }
    else {
        var shape_type_class = "circle";
    }

    for (var i = 0; i < n; i+=1){
        var size = Math.random() * (max - min) + min;
        var pos_x = Math.random() * (max_pos - min_pos) + min_pos;
        var pos_y = Math.random() * (max_pos - min_pos) + min_pos;

        var object = $('<div class="shape"></div>');

        var properties = `top: ${pos_x}px; right: ${pos_y}px; width: ${size}em; height: ${size}em;`;
        object.attr("style", properties);
        object.addClass(shape_type_class);

        $("#wrapper").append(object);
    }

}

$('.draw-button').click( function() {

    var number = $('#int-input').val();
    console.log(number);

    var type = $(this).attr('value');
    console.log(type);

    generate_shapes(number, type);
});

$("#wrapper").on('click', 'div.shape', function(){

    if ($(this).hasClass('selected-shape')) {
        $(this).remove();
    }
    else {
        $(this).addClass('selected-shape');
    }
   
});