$('#add-item-btn').click( function() {

    $("#collection").append($(`
            <div id="input-row" class="row flex bg-gray-100">
                <input class="col-title w-56" type="text">
                <input class="col-val w-56" type="text">
                <button class="to-top bg-white hover:bg-gray-100 text-gray-800 font-semibold py-1 px-3 border border-gray-400 rounded shadow">&uarr;</button>
                <button class="to-bottom bg-white hover:bg-gray-100 text-gray-800 font-semibold py-1 px-3 border border-gray-400 rounded shadow">&darr;</button>
                <button class="remove-btn bg-white hover:bg-gray-100 text-gray-800 font-semibold py-1 px-2 border border-gray-400 rounded shadow">&#9587;</button>
            </div>`));

});

$("#collection").on('click', 'button.remove-btn', function(){

    $(this).closest('.row').remove();

});

$("#collection").on('click', 'button.to-top', function(){

    var wrapper = $(this).closest('.row');
    wrapper.insertBefore(wrapper.prev());
});


$("#collection").on('click', 'button.to-bottom', function(){

    var wrapper = $(this).closest('.row');
    wrapper.insertAfter(wrapper.next());
});



$('#save-items-btn').click( function() {

    var result = {};

    var rows = $('#collection').children('.row');

    rows.each(function(){
        var key = $(this).children('.col-title').val();
        var value = $(this).children('.col-val').val();

        if (key != "" && value != "") {
            result[key] = value;
        }
    });

    $('.response').text(JSON.stringify(result));

});