
document.body.onload = addEasyNav();

function addEasyNav() {

    // a{begin..end}.html - file formats (^_^)
    var begin = 1, end = 15;
    var current = parseInt(document.title.substring(1));

    var prev = current-1, next = current+1;

    if (current <= begin) {
        prev = end;
    }
    if (current >= end) {
        next = begin;
    }

    var newDiv = document.createElement("div");
    newDiv.id = "easy-check";
    newDiv.innerHTML = `<a href="a${prev}.html">Prev</a><a href="a${next}.html">Next</a>`;

    document.body.appendChild(newDiv);
}