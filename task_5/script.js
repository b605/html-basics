// Вызов модального окна
$('.button').click( function() {
    $(this).parent().parent().css("background-color", "red");
    console.log(`.overlay.${$(this).attr('id')}`);
	$(`.overlay.${$(this).attr('id')}`).fadeIn();
});


// Закрытие окна на поле
$(document).mouseup( function (e) { 
	var popup = $('.popup');
	if (e.target != popup[0] && popup.has(e.target).length === 0){
		$('.overlay').fadeOut();
        $('.container').css("background-color", "aquamarine ");
	}
});
