// Прячем правую картинку
$('.button-left').click( function() {

    $('.left-img').removeClass('hidden');
    $('.right-img').addClass('hidden');

    $('.with-img').removeClass('w-1/2')

    $('.with-img.on-left').removeClass('collapsed');
    $('.with-img.on-right').removeClass('expanded');

    $('.with-img.on-left').addClass('expanded');
    $('.with-img.on-right').addClass('collapsed');
   
});

$('.button-both').click( function() {

    $('.with-img').addClass('w-1/2')

    $('.with-img').removeClass('collapsed');
    $('.with-img').removeClass('expanded');

    $('img').removeClass('hidden');
});

$('.button-right').click( function() {

    $('.left-img').addClass('hidden');
    $('.right-img').removeClass('hidden');

    $('.with-img').removeClass('w-1/2');

    $('.with-img.on-left').removeClass('expanded');
    $('.with-img.on-right').removeClass('collapsed');

    $('.with-img.on-left').addClass('collapsed');
    $('.with-img.on-right').addClass('expanded');
});